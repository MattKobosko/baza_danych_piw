/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package piwo;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 *
 * @author Administrator
 */
public class CStyles {
    private String Styl=null;
    private String Opis=null;
    private String img_src=null;
    private String img_desc=null;

    public CStyles(String Styl, String Opis, String img_src, String img_desc) {
        this.Styl=Styl;
        this.Opis=Opis;
        this.img_src=img_src;
        this.img_desc=img_desc;
        
    }
    
    public String getName(){
        return this.Styl;
    }
    
    public String getDesc(){
        return this.Opis;
    }
    
    public ImageIcon getImg(int w,int h){
        ImageIcon imageIcon = new ImageIcon(getClass().getResource("/img/style/"+this.img_src)); // load the image to a imageIcon
        Image image = imageIcon.getImage(); // transform it 
        Image newimg = image.getScaledInstance(w, h,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
        imageIcon = new ImageIcon(newimg);  // transform it back
        return imageIcon;
    }
        

    
    
    
    
    public String getImgDesc(){
        return this.img_desc;
    }
    
}
