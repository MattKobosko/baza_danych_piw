/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package piwo;

/**
 *
 * @author Administrator
 */
public class CRank implements Comparable<CRank>{
    private final int EID;
    private final String Name;
    private final Double Rank;
    

    public CRank(int EID, String Name, Double Rank) {
        this.EID=EID;
        this.Name = Name;
        this.Rank=Rank;
        
    }
    public Double getRank(){
        return Rank;
    }
    
    public String getName(){
        return Name;
    }

    @Override
    public String toString() {
        return "CRank{" + "Name=" + Name + ", Ocena=" + Rank + '}';
    }
    
    

    @Override
    public int compareTo(CRank o) {
        int ComparedRank=this.Rank.compareTo(o.getRank());
        if(ComparedRank==0){
            return EID;
        }
        else{
            return ComparedRank;
        }
    }
    
    
}
