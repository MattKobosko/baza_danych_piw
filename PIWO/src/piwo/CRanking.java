/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package piwo;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
/**
 *
 * @author Administrator
 */
public class CRanking {
    private Connection conn=null;

    public CRanking() {
        this.lista = new ArrayList();
        conn=sqliteConnection.dbConnector();
    }
    
    private final ArrayList<CRank> lista;
    
    
    public int getAll(){
        int howMany=0;
        try{
            String query="select * from Ranking ";
            ResultSet rs;
            try(PreparedStatement pst=conn.prepareStatement(query)){
                rs=pst.executeQuery();
                while(rs.next()){
                    //new CRank(Integer.parseInt(rs.getObject(1).toString()),rs.getObject(2).toString(),Double.parseDouble(rs.getObject(4).toString()),Double.parseDouble(rs.getObject(5).toString()),Integer.parseInt(rs.getObject(6).toString()));
                    //lista.add(new CRank(Integer.parseInt(rs.getObject(1).toString()),rs.getObject(2).toString(),Double.parseDouble(rs.getObject(4).toString()),Double.parseDouble(rs.getObject(5).toString()),Integer.parseInt(rs.getObject(6).toString())));
                    lista.add(new CRank(Integer.parseInt(rs.getObject(1).toString()),rs.getObject(2).toString(),Double.parseDouble(rs.getObject(3).toString())));
                    howMany++;
                }
                
                Collections.sort(lista);
                Collections.reverse(lista);
            }
            
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null,e);
        }
        return howMany;
    }  
    public String getName(int index){
        return lista.get(index).getName();
        }
    public Double getRank(int index){
        return lista.get(index).getRank();
    }
    
    public ImageIcon settedIcon(int index){
        switch (index) {
            case 1:
                return new ImageIcon(getClass().getClassLoader().getResource("img/stars/star.png"));
            case 2:
                return new ImageIcon(getClass().getClassLoader().getResource("img/stars/half-star.png"));
            default:
                return new ImageIcon(getClass().getClassLoader().getResource("img/stars/no-star.png"));
        }
    }
    public ImageIcon[] settedIcons(Double Rank){
        ImageIcon[] icons=new ImageIcon[5];
        if(Rank<=1){
            icons[0]=settedIcon(2);icons[1]=settedIcon(3);icons[2]=settedIcon(3);icons[3]=settedIcon(3);icons[4]=settedIcon(3);
        }
        if(Rank>1&&Rank<=2){
            icons[0]=settedIcon(1);icons[1]=settedIcon(3);icons[2]=settedIcon(3);icons[3]=settedIcon(3);icons[4]=settedIcon(3);
        }
        if(Rank>2&&Rank<=3){
            icons[0]=settedIcon(1);icons[1]=settedIcon(2);icons[2]=settedIcon(3);icons[3]=settedIcon(3);icons[4]=settedIcon(3);
        }
        if(Rank>3&&Rank<=4){
            icons[0]=settedIcon(1);icons[1]=settedIcon(1);icons[2]=settedIcon(3);icons[3]=settedIcon(3);icons[4]=settedIcon(3);
        }
        if(Rank>4&&Rank<=5){
            icons[0]=settedIcon(1);icons[1]=settedIcon(1);icons[2]=settedIcon(2);icons[3]=settedIcon(3);icons[4]=settedIcon(3);
        }
        if(Rank>5&&Rank<=6){
            icons[0]=settedIcon(1);icons[1]=settedIcon(1);icons[2]=settedIcon(1);icons[3]=settedIcon(3);icons[4]=settedIcon(3);
        }
        if(Rank>6&&Rank<=7){
            icons[0]=settedIcon(1);icons[1]=settedIcon(1);icons[2]=settedIcon(1);icons[3]=settedIcon(2);icons[4]=settedIcon(3);
        }
        if(Rank>7&&Rank<=8){
            icons[0]=settedIcon(1);icons[1]=settedIcon(1);icons[2]=settedIcon(1);icons[3]=settedIcon(1);icons[4]=settedIcon(3);
        }
        if(Rank>8&&Rank<=9){
            icons[0]=settedIcon(1);icons[1]=settedIcon(1);icons[2]=settedIcon(1);icons[3]=settedIcon(1);icons[4]=settedIcon(2);
        }
        if(Rank>9){
            icons[0]=settedIcon(1);icons[1]=settedIcon(1);icons[2]=settedIcon(1);icons[3]=settedIcon(1);icons[4]=settedIcon(1);
        }
        return icons;
    }
}
